package buu.chaichana.androidtask1

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import java.util.*

class MainActivity : AppCompatActivity() {
    var correct:Int = 0
    var incorrect:Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        game(correct, incorrect)
    }

    fun game(correctScore: Int, incorrectScore: Int) {
        val correctTxtView = findViewById<TextView>(R.id.correctTxtView)
        correctTxtView.setText("Correct: " + correct.toString())
        val incorrectTxtView = findViewById<TextView>(R.id.incorrectTxtView)
        incorrectTxtView.setText("Incorrect: " + incorrect.toString())

        val firstRandTxt = findViewById<TextView>(R.id.firstRandTxt)
        val secondRandTxt = findViewById<TextView>(R.id.secondRandTxt)
        val alertTxtView = findViewById<TextView>(R.id.alertTxtView)

        val btnFirst = findViewById<Button>(R.id.btnFirst)
        val btnSecond = findViewById<Button>(R.id.btnSecond)
        val btnThird = findViewById<Button>(R.id.btnThird)

        val firstRandNumber: Int = Random().nextInt(9) + 1
        val secondRandNumber: Int = Random().nextInt(9) + 1
        val ansNumber = firstRandNumber + secondRandNumber
        //Toast.makeText(MainActivity@this,"answer is " + ansNumber, Toast.LENGTH_SHORT).show()

        val firstPseudo = ansNumber + 1
        val secondPseudo = ansNumber - 1

        val randomChoice: Int = Random().nextInt(2) + 1
        if (randomChoice == 1) {
            btnFirst.setText(ansNumber.toString())
            btnSecond.setText(firstPseudo.toString())
            btnThird.setText(secondPseudo.toString())
        }
        else if (randomChoice == 2) {
            btnSecond.setText(ansNumber.toString())
            btnThird.setText(firstPseudo.toString())
            btnFirst.setText(secondPseudo.toString())
        }
        else {
            btnThird.setText(ansNumber.toString())
            btnFirst.setText(firstPseudo.toString())
            btnSecond.setText(secondPseudo.toString())
        }

        alertTxtView.setVisibility(View.INVISIBLE)
        firstRandTxt.text = firstRandNumber.toString()
        secondRandTxt.text = secondRandNumber.toString()

        btnFirst.setOnClickListener {
            if (btnFirst.text.equals(ansNumber.toString())){
                alertTxtView.setVisibility(View.VISIBLE)
                alertTxtView.setText("Correct!")
                alertTxtView.setTextColor(Color.GREEN)
                correct += 1
                correctTxtView.setText("Correct: " + correct.toString())
                Handler().postDelayed({
                    game(correct, incorrect)
                }, 1000)

            }
            else {
                alertTxtView.setVisibility(View.VISIBLE)
                alertTxtView.setText("Incorrect!")
                alertTxtView.setTextColor(Color.RED)
                incorrect += 1
                incorrectTxtView.setText("Incorrect: " + incorrect.toString())
            }
        }
        btnSecond.setOnClickListener {
            if (btnSecond.text.equals(ansNumber.toString())){
                alertTxtView.setVisibility(View.VISIBLE)
                alertTxtView.setText("Correct!")
                alertTxtView.setTextColor(Color.GREEN)
                correct += 1
                correctTxtView.setText("Correct: " + correct.toString())
                Handler().postDelayed({
                        game(correct, incorrect)
                }, 1000)
            }
            else {
                alertTxtView.setVisibility(View.VISIBLE)
                alertTxtView.setText("Incorrect!")
                alertTxtView.setTextColor(Color.RED)
                incorrect += 1
                incorrectTxtView.setText("Incorrect: " + incorrect.toString())
            }
        }
        btnThird.setOnClickListener {
            if (btnThird.text.equals(ansNumber.toString())){
                alertTxtView.setVisibility(View.VISIBLE)
                alertTxtView.setText("Correct!")
                alertTxtView.setTextColor(Color.GREEN)
                correct += 1
                correctTxtView.setText("Correct: " + correct.toString())
                Handler().postDelayed({
                    game(correct, incorrect)
                }, 1000)
            }
            else {
                alertTxtView.setVisibility(View.VISIBLE)
                alertTxtView.setText("Incorrect!")
                alertTxtView.setTextColor(Color.RED)
                incorrect += 1
                incorrectTxtView.setText("Incorrect: " + incorrect.toString())
            }
        }
    }

}